<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

    Route ::group (['namespace' => 'management' , 'prefix' => '/v1/management'],function (){
        Route ::resource('/leaveApprove','LeaveApproveController');
        Route ::resource('/leaveDetail','LeaveDetailController');
        Route ::resource('/leavePolicy','LeavePolicyController');
        Route ::resource('/leavePolicyDetail','LeavePolicyDetailController');
        Route ::resource('/leaveRequest','LeaveRequestController');
        Route ::resource('/overtimeApprove','OvertimeApproveController');
        Route ::resource('/overtimePolicy','OvertimePolicyController');
        Route ::resource('/overtimeRequest','OvertimeRequestController');
        Route ::resource('/payruleDetail','PayruleDetailController');
        Route ::resource('/payrulePolicy','PayrulePolicyController');
        Route ::resource('/payrulePolicyMapping','PayrulePolicyMappingController');


    });


