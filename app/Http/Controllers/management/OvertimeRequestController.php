<?php

namespace App\Http\Controllers\management;

use datetime;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class OvertimeRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response() -> json(["index,overtimeRequest"],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $OvertimeRequest = new \App\Entities\OvertimeRequest();
        $OvertimeRequest->setCompanyId($request->company_id);
        $OvertimeRequest->setProfileId($request->profile_id);
        $OvertimeRequest->setOvertimeRequestId($request->overtime_request_id);
        $OvertimeRequest->setOvertimeRequestWorkDate($request->overtime_request_work_date);
        $OvertimeRequest->setOvertimeRequestNo($request->overtime_request_no);
        $OvertimeRequest->setOvertimeRequestDescription($request->overtime_request_description);
        $OvertimeRequest->setOvertimeRequestMinute($request->overtime_request_minute);
        $OvertimeRequest->setOvertimeRequestRate($request->overtime_request_rate);
        $OvertimeRequest->setOvertimeRequestApproverCount($request->overtime_request_approver_count);
        $OvertimeRequest->setOvertimeRequestTotalApprover($request->overtime_request_total_approver);
        $OvertimeRequest->setOvertimeRequestedStatus('P');
        $OvertimeRequest->setOvertimeRequestedDate(new DateTime('now'));
        $OvertimeRequest->setOvertimeRequestedBy($request->overtime_requested_by);
        $OvertimeRequest->setOvertimeRequestedByIp(\App\Services\ServiceIp::get_client_ip());
        $OvertimeRequest->setOvertimeRejectedDate(new DateTime('now'));
        $OvertimeRequest->setOvertimeRejectedBy($request->overtime_rejected_by);
        $OvertimeRequest->setOvertimeRejectedByIp(\App\Services\ServiceIp::get_client_ip());



        app('em')->persist($OvertimeRequest);
        app('em')->flush();

        return response()->json(['success'=>true],200);
    }

    /**
     * Display the specified resource.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        $OvertimeRequest = app('em')
            ->createQueryBuilder()
            ->select('o')
            ->from('\\App\\Entities\\OvertimeRequest','o')
            ->where('o.company_id ='.$id)
            ->andWhere('o.profile_id ='.$request->profile_id)
            ->getQuery()
            ->getOneOrNullResult();
        $response = [

            'overtime_request_id' => $OvertimeRequest->getOvertimeRequestId(),
            'overtime_request_work_date' => $OvertimeRequest->getOvertimeRequestWorkDate(),
            'overtime_request_no' => $OvertimeRequest->getOvertimeRequestNo(),
            'overtime_request_description' => $OvertimeRequest->getOvertimeRequestDescription(),
            'overtime_request_minute' => $OvertimeRequest->getOvertimeRequestMinute(),
            'overtime_request_rate' => $OvertimeRequest->getOvertimeRequestRate(),
            'overtime_request_approver_count' => $OvertimeRequest->getOvertimeRequestApproverCount(),
            'overtime_request_total_approver' => $OvertimeRequest->getOvertimeRequestTotalApprover(),
            'overtime_requested_status' => $OvertimeRequest->getOvertimeRequestedStatus(),
            'overtime_requested_date' => $OvertimeRequest->getOvertimeRequestedDate(),
            'overtime_requested_by' => $OvertimeRequest->getOvertimeRequestedBy(),
            'overtime_requested_by_ip' => $OvertimeRequest->getOvertimeRequestedByIp(),
            'overtime_rejected_date' => $OvertimeRequest->getOvertimeRejectedDate(),
            'overtime_rejected_by' => $OvertimeRequest->getOvertimeRejectedBy(),
            'overtime_rejected_by_ip' => $OvertimeRequest->getOvertimeRejectedByIp(),

           

           
        ];
        return response()->json($response,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $OvertimeRequest = app('em')
            ->createQueryBuilder()
            ->select('o')
            ->from('\\App\\Entities\\OvertimeRequest','o')
            ->where('o.company_id ='.$id)
            ->andWhere('o.profile_id ='.$request->profile_id)
            ->getQuery()
            ->getOneOrNullResult();


        $OvertimeRequest->setOvertimeRequestId($request->overtime_request_id);
        $OvertimeRequest->setOvertimeRequestWorkDate($request->overtime_request_work_date);
        $OvertimeRequest->setOvertimeRequestNo($request->overtime_request_no);
        $OvertimeRequest->setOvertimeRequestDescription($request->overtime_request_description);
        $OvertimeRequest->setOvertimeRequestMinute($request->overtime_request_minute);
        $OvertimeRequest->setOvertimeRequestRate($request->overtime_request_rate);
        $OvertimeRequest->setOvertimeRequestApproverCount($request->overtime_request_approver_count);
        $OvertimeRequest->setOvertimeRequestTotalApprover($request->overtime_request_total_approver);
        $OvertimeRequest->setOvertimeRequestedStatus($request->overtime_requested_status);
        if  ($request->overtime_requested_status =='R'){

                $OvertimeRequest->setOvertimeRejectedDate(new DateTime('now'));
                $OvertimeRequest->setOvertimeRejectedBy($request->overtime_rejected_by);
                $OvertimeRequest->setOvertimeRejectedByIp(\App\Services\ServiceIp::get_client_ip());
        }


        app('em')->persist($OvertimeRequest);
        app('em')->flush();
        return response()->json(['success'=>true],200);
    }

    /**
     * Remove the specified resource from storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $OvertimeRequest = app('em')
            ->createQueryBuilder()
            ->select('o')
            ->from('\\App\\Entities\\OvertimeRequest','o')
            ->where('o.company_id ='.$id)
            ->andWhere('o.profile_id ='.$request->profile_id)
            ->getQuery()
            ->getOneOrNullResult();

        if($OvertimeRequest!= null) app('em')->remove($OvertimeRequest);

        app('em') ->flush();
        return response()->json(['delete success'],200);
    }
}
