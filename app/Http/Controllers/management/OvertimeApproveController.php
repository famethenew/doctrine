<?php
namespace App\Http\Controllers\management;
use datetime;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class OvertimeApproveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = app('em')->getRepository('\App\Entities\OvertimeApprove')->findAll();
        
        return response() -> json($list,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $OvertimeApprove = new \App\Entities\OvertimeApprove();
        $OvertimeApprove->setCompanyId($request->company_id);
        $OvertimeApprove->setOvertimeRequestId($request->overtime_request_id);
        $OvertimeApprove->setSeq($request->seq);
        $OvertimeApprove->setApprovedDate(new DateTime('now'));
        $OvertimeApprove->setApprovedBy($request->approved_by);
        $OvertimeApprove->setApprovedByIp(\App\Services\ServiceIp::get_client_ip());


        app('em')->persist($OvertimeApprove);
        app('em')->flush();

        return response()->json(['success'=>true],200);
    }

    /**
     * Display the specified resource.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        $OvertimeApprove = app('em')
            ->createQueryBuilder()
            ->select('o')
            ->from('\\App\\Entities\\OvertimeApprove','o')
            ->where('o.company_id ='.$id)
            ->andWhere('o.overtime_request_id ='.$request->overtime_request_id)
            ->getQuery()
            ->getOneOrNullResult();
        $response = [

            'seq' => $OvertimeApprove->getSeq(),
            'approved_date' => $OvertimeApprove->getApprovedDate(),
            'approved_by' => $OvertimeApprove->getApprovedBy(),
            'approved_by_ip' => $OvertimeApprove->getApprovedByIp()
        ];
        return response()->json($response,200);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $OvertimeApprove = app ('em')
            ->createQueryBuilder()
            ->select('o')
            ->from('App\\Entities\\OvertimeApprove','o')
            ->where('o.company_id ='.$id)
            ->andWhere('o.overtime_request_id ='.$request->overtime_request_id)
            ->getQuery()
            ->getOneorNullResult();


        $OvertimeApprove->setSeq($request->seq);
        $OvertimeApprove->setApprovedDate(new DateTime('now'));
        $OvertimeApprove->setApprovedBy($request->approved_by);
        $OvertimeApprove->setApprovedByIp(\App\Services\ServiceIp::get_client_ip());

        app('em')->persist($OvertimeApprove);

        app('em')->flush();

        return response()->json(['success'=>true],200);
    }

    /**
     * Remove the specified resource from storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $OvertimeApprove = app ('em')
            ->createQueryBuilder()
            ->select('o')
            ->from('App\\Entities\\OvertimeApprove','o')
            ->where('o.company_id ='.$id)
            ->andWhere('o.overtime_request_id ='.$request->overtime_request_id)
            ->getQuery()
            ->getOneorNullResult();

        if($OvertimeApprove!= null) app('em')->remove($OvertimeApprove);

        app('em') ->flush();
        return response()->json(['delete success'],200);
    }
}
