<?php

namespace App\Http\Controllers\management;

use datetime;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OvertimePolicyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response() -> json(["index,overtimeApprove"],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $OvertimePolicy = new \App\Entities\OvertimePolicy();
        $OvertimePolicy->setCompanyId($request->company_id);
        $OvertimePolicy->setOvertimePolicyId($request->overtime_policy_id);
        $OvertimePolicy->setOvertimePolicyName($request->overtime_policy_name);
        $OvertimePolicy->setOvertimePolicyDescription($request->overtime_policy_description);
        $OvertimePolicy->setOvertimePolicyWorkOt($request->overtime_policy_work_ot);
        $OvertimePolicy->setOvertimePolicyWorkHoliday($request->overtime_policy_work_holiday);
        $OvertimePolicy->setOvertimePolicyWorkHolidayOt($request->overtime_policy_work_holiday_ot);
        $OvertimePolicy->setOvertimePolicyMinMinute($request->overtime_policy_min_minute);
        $OvertimePolicy->setOvertimePolicyMaxMinute($request->overtime_policy_max_minute);
        $OvertimePolicy->setOvertimePolicyMinuteTimeRange($request->overtime_policy_minute_time_range);
        $OvertimePolicy->setOvertimePolicyMinuteTimeFull($request->overtime_policy_minute_time_full);
        $OvertimePolicy->setOvertimePolicyBreakOver($request->overtime_policy_break_over);
        $OvertimePolicy->setOvertimePolicyBreakMinute($request->overtime_policy_break_minute);
        $OvertimePolicy->setOvertimePolicyApproveAuto($request->overtime_policy_approve_auto);
        $OvertimePolicy->setOvertimePolicyApproveAutoMinute($request->overtime_policy_approve_auto_minute);
        $OvertimePolicy->setOvertimePolicyBeforeWork($request->overtime_policy_before_work);
        $OvertimePolicy->setOvertimePolicyAfterWork($request->overtime_policy_after_work);
        $OvertimePolicy->setOvertimePolicyStatus('1');
        $OvertimePolicy->setOvertimePolicyUsageFlag($request->overtime_policy_usage_flag);
        $OvertimePolicy->setOvertimePolicyApproveType($request->overtime_policy_approve_type);
        $OvertimePolicy->setOvertimePolicyApproveCount($request->overtime_policy_approve_count);
        $OvertimePolicy->setCreatedDate(new DateTime('now'));
        $OvertimePolicy->setCreatedBy($request->created_by);
        $OvertimePolicy->setCreatedByIp(\App\Services\ServiceIp::get_client_ip());


        app('em')->persist($OvertimePolicy);
        app('em')->flush();

        return response()->json(['success'=>true],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $OvertimePolicy = app ('em')
            ->getRepository('\\App\\Entities\\OvertimePolicy')
            ->find($id);
        $response = [

            'overtime_policy_id' => $OvertimePolicy->getOvertimePolicyId(),
            'overtime_policy_name' => $OvertimePolicy->getOvertimePolicyName(),
            'overtime_policy_description' => $OvertimePolicy->getOvertimePolicyDescription(),
            'overtime_policy_work_ot' => $OvertimePolicy->getOvertimePolicyWorkOt(),
            'overtime_policy_work_holiday' => $OvertimePolicy->getOvertimePolicyWorkHoliday(),
            'overtime_policy_work_holiday_ot' => $OvertimePolicy->getOvertimePolicyWorkHolidayOt(),
            'overtime_policy_min_minute' => $OvertimePolicy->getOvertimePolicyMinMinute(),
            'overtime_policy_max_minute' => $OvertimePolicy->getOvertimePolicyMaxMinute(),
            'overtime_policy_minute_time_range' => $OvertimePolicy->getOvertimePolicyMinuteTimeRange(),
            'overtime_policy_minute_time_full' => $OvertimePolicy->getOvertimePolicyMinuteTimeFull(),
            'overtime_policy_break_over' => $OvertimePolicy->getOvertimePolicyBreakOver(),
            'overtime_policy_break_minute' => $OvertimePolicy->getOvertimePolicyBreakMinute(),
            'overtime_policy_approve_auto' => $OvertimePolicy->getOvertimePolicyApproveAuto(),
            'overtime_policy_approve_auto_minute' => $OvertimePolicy->getOvertimePolicyApproveAutoMinute(),
            'overtime_policy_before_work' => $OvertimePolicy->getOvertimePolicyBeforeWork(),
            'overtime_policy_after_work' => $OvertimePolicy->getOvertimePolicyAfterWork(),
            'overtime_policy_status' => $OvertimePolicy->getOvertimePolicyStatus(),
            'overtime_policy_usage_flag' => $OvertimePolicy->getOvertimePolicyUsageFlag(),
            'overtime_policy_approve_type' => $OvertimePolicy->getOvertimePolicyApproveType(),
            'overtime_policy_approve_count' => $OvertimePolicy->getOvertimePolicyApproveCount(),
            'created_date' => $OvertimePolicy->getCreatedDate(),
            'created_by' => $OvertimePolicy->getCreatedBy(),
            'created_by_ip' => $OvertimePolicy->getCreatedByIp(),
            'updated_date' => $OvertimePolicy->getUpdatedDate(),
            'updated_by' => $OvertimePolicy->getUpdatedBy(),
            'updated_by_ip' => $OvertimePolicy->getUpdatedByIp(),
            'deleted_date' => $OvertimePolicy->getDeletedDate(),
            'deleted_by' => $OvertimePolicy->getDeletedBy(),
            'deleted_by_ip' => $OvertimePolicy->getDeletedByIp(),

        ];
        return response()->json($response,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $OvertimePolicy = app ('em')
            ->getRepository('\\App\\Entities\\OvertimePolicy')
            ->find($id);
        $OvertimePolicy->setOvertimePolicyId($request->overtime_policy_id);
        $OvertimePolicy->setOvertimePolicyName($request->overtime_policy_name);
        $OvertimePolicy->setOvertimePolicyDescription($request->overtime_policy_description);
        $OvertimePolicy->setOvertimePolicyWorkOt($request->overtime_policy_work_ot);
        $OvertimePolicy->setOvertimePolicyWorkHoliday($request->overtime_policy_work_holiday);
        $OvertimePolicy->setOvertimePolicyWorkHolidayOt($request->overtime_policy_work_holiday_ot);
        $OvertimePolicy->setOvertimePolicyMinMinute($request->overtime_policy_min_minute);
        $OvertimePolicy->setOvertimePolicyMaxMinute($request->overtime_policy_max_minute);
        $OvertimePolicy->setOvertimePolicyMinuteTimeRange($request->overtime_policy_minute_time_range);
        $OvertimePolicy->setOvertimePolicyMinuteTimeFull($request->overtime_policy_minute_time_full);
        $OvertimePolicy->setOvertimePolicyBreakOver($request->overtime_policy_break_over);
        $OvertimePolicy->setOvertimePolicyBreakMinute($request->overtime_policy_break_minute);
        $OvertimePolicy->setOvertimePolicyApproveAuto($request->overtime_policy_approve_auto);
        $OvertimePolicy->setOvertimePolicyApproveAutoMinute($request->overtime_policy_approve_auto_minute);
        $OvertimePolicy->setOvertimePolicyBeforeWork($request->overtime_policy_before_work);
        $OvertimePolicy->setOvertimePolicyAfterWork($request->overtime_policy_after_work);
        $OvertimePolicy->setOvertimePolicyStatus($request->overtime_policy_status);
        $OvertimePolicy->setOvertimePolicyUsageFlag($request->overtime_policy_usage_flag);
        $OvertimePolicy->setOvertimePolicyApproveType($request->overtime_policy_approve_type);
        $OvertimePolicy->setOvertimePolicyApproveCount($request->overtime_policy_approve_count);
        $OvertimePolicy->setUpdatedDate(new DateTime('now'));
        $OvertimePolicy->setUpdatedBy($request->updated_by);
        $OvertimePolicy->setUpdatedByIp(\App\Services\ServiceIp::get_client_ip());

        app('em')->persist($OvertimePolicy);

        app('em')->flush();

        return response()->json(['success'=>true],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $OvertimePolicy = app ('em')
            ->getRepository('\\App\\Entities\\OvertimePolicy')
            ->find($id);

        $OvertimePolicy->setOvertimePolicyStatus('5');
        $OvertimePolicy->setDeletedDate($request->deleted_date);
        $OvertimePolicy->setDeletedBy($request->deleted_by);
        $OvertimePolicy->setDeletedByIp(\App\Services\ServiceIp::get_client_ip());

        app('em') ->flush();
        return response()->json(['delete success'],200);
    }
}
