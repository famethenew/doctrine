<?php

namespace App\Http\Controllers\management;

use datetime;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PayruleDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response() -> json(["index,overtimeApprove"],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $PayruleDetail = new \App\Entities\PayruleDetail();
        $PayruleDetail->setCompanyId($request->company_id);
        $PayruleDetail->setPayruleDetailId($request->payrule_detail_id);
        $PayruleDetail->setCloseDate(new DateTime('now'));    // ใส่ DATETIME  becouse RUNNING
        $PayruleDetail->setPaymentDate(new DateTime('now'));   // ใส่ DATETIME  becouse RUNNING
        $PayruleDetail->setPayruleDetailStatus('1');
        $PayruleDetail->setCreatedDate(new DateTime('now'));
        $PayruleDetail->setCreatedBy($request->created_by);
        $PayruleDetail->setCreatedByIp(\App\Services\ServiceIp::get_client_ip());




        app('em')->persist($PayruleDetail);
        app('em')->flush();

        return response()->json(['success'=>true],200);
    }

    /**
     * Display the specified resource.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        $PayruleDetail= app('em')
            ->createQueryBuilder()
            ->select('o')
            ->from('App\\Entities\\PayruleDetail','o')
            ->where('o.company_id ='.$id)
            ->andWhere('o.payrule_detail_id ='.$request->payrule_detail_id)
            ->getQuery()
            ->getOneorNullResult();
        $response = [

            'close_date' => $PayruleDetail->getCloseDate(),
            'payment_date' => $PayruleDetail->getPaymentDate(),
            'payrule_detail_status' => $PayruleDetail->getPayruleDetailStatus(),
            'created_date' => $PayruleDetail->getCreatedDate(),
            'created_by' => $PayruleDetail->getCreatedBy(),
            'created_by_ip' => $PayruleDetail->getCreatedByIp(),
            'updated_date' => $PayruleDetail->getUpdatedDate(),
            'updated_by' => $PayruleDetail->getUpdatedBy(),
            'updated_by_ip' => $PayruleDetail->getUpdatedByIp(),
            'deleted_date' => $PayruleDetail->getDeletedDate(),
            'deleted_by' => $PayruleDetail->getDeletedBy(),
            'deleted_by_ip' => $PayruleDetail->getDeletedByIp(),



        ];
        return response()->json($response,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $PayruleDetail= app('em')
            ->createQueryBuilder()
            ->select('o')
            ->from('App\\Entities\\PayruleDetail','o')
            ->where('o.company_id ='.$id)
            ->andWhere('o.payrule_detail_id ='.$request->payrule_detail_id)
            ->getQuery()
            ->getOneorNullResult();

        $PayruleDetail->setCloseDate($request->close_date);
        $PayruleDetail->setPaymentDate($request->payment_date);
        $PayruleDetail->setPayruleDetailStatus($request->payrule_detail_status);
        $PayruleDetail->setUpdatedDate(new DateTime('now'));
        $PayruleDetail->setUpdatedBy($request->updated_by);
        $PayruleDetail->setUpdatedByIp(\App\Services\ServiceIp::get_client_ip());

        app('em')->persist($PayruleDetail);

        app('em')->flush();

        return response()->json(['success'=>true],200);
    }

    /**
     * Remove the specified resource from storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $PayruleDetail = app ('em')
            ->createQueryBuilder()
            ->select('o')
            ->from('App\\Entities\\PayruleDetail','o')
            ->where('o.company_id ='.$id)
            ->andWhere('o.payrule_detail_id ='.$request->payrule_detail_id)
            ->getQuery()
            ->getOneorNullResult();

        $PayruleDetail->setPayruleDetailStatus('5');
        $PayruleDetail->setDeletedDate(new DateTime('now'));
        $PayruleDetail->setDeletedBy($request->deleted_by);
        $PayruleDetail->setDeletedByIp(\App\Services\ServiceIp::get_client_ip());

        app('em') ->flush();
        return response()->json(['delete success'],200);

    }
}
