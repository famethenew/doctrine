<?php

namespace App\Http\Controllers\management;

use datetime;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PayrulePolicyMappingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response() -> json(["index,overtimeApprove"],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $PayrulePolicyMapping = new \App\Entities\PayrulePolicyMapping();
        $PayrulePolicyMapping->setCompanyId($request->company_id);
        $PayrulePolicyMapping->setPayrulePolicyId($request->payrule_policy_id);
        $PayrulePolicyMapping->setPayruleDetailId($request->payrule_detail_id);
        $PayrulePolicyMapping->setMappedDate(new DateTime('now'));
        $PayrulePolicyMapping->setMappedBy($request->mapped_by);
        $PayrulePolicyMapping->setMappedByIp(\App\Services\ServiceIp::get_client_ip());

        app('em')->persist($PayrulePolicyMapping);
        app('em')->flush();

        return response()->json(['success'=>true],200);

    }

    /**
     * Display the specified resource.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        $PayrulePolicyMapping = app ('em')
            ->createQueryBuilder()
            ->select('o')
            ->from('App\\Entities\\PayrulePolicyMapping','o')
            ->where('o.company_id ='.$id)
            ->andWhere('o.payrule_policy_id ='.$request->payrule_policy_id)
            ->andWhere('o.payrule_detail_id ='.$request->payrule_detail_id)
            ->getQuery()
            ->getOneorNullResult();

        $response = [

            'mapped_date' => $PayrulePolicyMapping->getMappedDate(),
            'mapped_by' => $PayrulePolicyMapping->getMappedBy(),
            'mapped_by_ip' => $PayrulePolicyMapping->getMappedByIp(),



        ];
        return response()->json($response,200);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $PayrulePolicyMapping = app ('em')
            ->createQueryBuilder()
            ->select('o')
            ->from('App\\Entities\\PayrulePolicyMapping','o')
            ->where('o.company_id ='.$id)
            ->andWhere('o.payrule_policy_id ='.$request->payrule_policy_id)
            ->andWhere('o.payrule_detail_id ='.$request->payrule_detail_id)
            ->getQuery()
            ->getOneorNullResult();


        $PayrulePolicyMapping->setMappedDate(new DateTime('now'));
        $PayrulePolicyMapping->setMappedBy($request->mapped_by);
        $PayrulePolicyMapping->setMappedByIp(\App\Services\ServiceIp::get_client_ip());



        app('em')->persist($PayrulePolicyMapping);

        app('em')->flush();

        return response()->json(['success'=>true],200);
    }

    /**
     * Remove the specified resource from storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $PayrulePolicyMapping = app ('em')
            ->createQueryBuilder()
            ->select('o')
            ->from('App\\Entities\\PayrulePolicyMapping','o')
            ->where('o.company_id ='.$id)
            ->andWhere('o.payrule_policy_id ='.$request->payrule_policy_id)
            ->andWhere('o.payrule_detail_id ='.$request->payrule_detail_id)
            ->getQuery()
            ->getOneorNullResult();

        if($PayrulePolicyMapping!= null) app('em')->remove($PayrulePolicyMapping);

        app('em') ->flush();
        return response()->json(['delete success'],200);
    }
}
