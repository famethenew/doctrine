<?php
namespace App\Entities;
use Doctrine\ORM\Mapping AS ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="TB_PAYRULE_POLICY_MAPPING")
 */

class PayrulePolicyMapping
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer" , length=11 )
     */
    protected $company_id;
    /**
     * @ORM\Id
     * @ORM\Column(type="integer" , length=11 )
     */
    protected $payrule_policy_id;
    /**
     * @ORM\Id
     * @ORM\Column(type="integer" , length=11 )
     */
    protected $payrule_detail_id;    /**

     * @ORM\Column(type="datetime" , nullable = true)
     */
    protected $mapped_date;
    /**

     * @ORM\Column(type="integer" , length=11 , nullable = true)
     */
    protected $mapped_by;
    /**
     * @ORM\Column(type="string" , length= 30, nullable = true)
     */
    protected $mapped_by_ip;

    /**
     * @return mixed
     */
    public function getCompanyId()
    {
        return $this->company_id;
    }

    /**
     * @param mixed $company_id
     */
    public function setCompanyId($company_id)
    {
        $this->company_id = $company_id;
    }

    /**
     * @return mixed
     */
    public function getPayrulePolicyId()
    {
        return $this->payrule_policy_id;
    }

    /**
     * @param mixed $payrule_policy_id
     */
    public function setPayrulePolicyId($payrule_policy_id)
    {
        $this->payrule_policy_id = $payrule_policy_id;
    }

    /**
     * @return mixed
     */
    public function getPayruleDetailId()
    {
        return $this->payrule_detail_id;
    }

    /**
     * @param mixed $payrule_detail_id
     */
    public function setPayruleDetailId($payrule_detail_id)
    {
        $this->payrule_detail_id = $payrule_detail_id;
    }

    /**
     * @return mixed
     */
    public function getMappedDate()
    {
        return $this->mapped_date;
    }

    /**
     * @param mixed $mapped_date
     */
    public function setMappedDate($mapped_date)
    {
        $this->mapped_date = $mapped_date;
    }

    /**
     * @return mixed
     */
    public function getMappedBy()
    {
        return $this->mapped_by;
    }

    /**
     * @param mixed $mapped_by
     */
    public function setMappedBy($mapped_by)
    {
        $this->mapped_by = $mapped_by;
    }

    /**
     * @return mixed
     */
    public function getMappedByIp()
    {
        return $this->mapped_by_ip;
    }

    /**
     * @param mixed $mapped_by_ip
     */
    public function setMappedByIp($mapped_by_ip)
    {
        $this->mapped_by_ip = $mapped_by_ip;
    }


}