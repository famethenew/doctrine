<?php
namespace App\Entities;
use Doctrine\ORM\Mapping AS ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="TB_LEAVE_APPROVE")
 */

class LeaveApprove
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer" , length=11)
     */
    protected $company_id;
    /**
     * @ORM\Id
     * @ORM\Column(type="integer" , length=11)
     */
    protected $leave_request_id;
    /**
     * @ORM\Column(type="integer" , length=11, nullable = true)
     */
    protected  $seq;
    /**
     * @ORM\Column(type="datetime" )
     */
    protected  $approved_date;
    /**
     * @ORM\Column(type="integer" , length=11, nullable = true)
     */
    protected  $approved_by;
    /**
     * @ORM\Column(type="string" , length=30, nullable = true)
     */
    protected  $approved_by_ip;

    /**
     * @return mixed
     */
    public function getCompanyId()
    {
        return $this->company_id;
    }

    /**
     * @param mixed $company_id
     */
    public function setCompanyId($company_id)
    {
        $this->company_id = $company_id;
    }

    /**
     * @return mixed
     */
    public function getLeaveRequestId()
    {
        return $this->leave_request_id;
    }

    /**
     * @param mixed $leave_request_id
     */
    public function setLeaveRequestId($leave_request_id)
    {
        $this->leave_request_id = $leave_request_id;
    }

    /**
     * @return mixed
     */
    public function getSeq()
    {
        return $this->seq;
    }

    /**
     * @param mixed $seq
     */
    public function setSeq($seq)
    {
        $this->seq = $seq;
    }

    /**
     * @return mixed
     */
    public function getApprovedDate()
    {
        return $this->approved_date;
    }

    /**
     * @param mixed $approved_date
     */
    public function setApprovedDate($approved_date)
    {
        $this->approved_date = $approved_date;
    }

    /**
     * @return mixed
     */
    public function getApprovedBy()
    {
        return $this->approved_by;
    }

    /**
     * @param mixed $approved_by
     */
    public function setApprovedBy($approved_by)
    {
        $this->approved_by = $approved_by;
    }

    /**
     * @return mixed
     */
    public function getApprovedByIp()
    {
        return $this->approved_by_ip;
    }

    /**
     * @param mixed $approved_by_ip
     */
    public function setApprovedByIp($approved_by_ip)
    {
        $this->approved_by_ip = $approved_by_ip;
    }

}