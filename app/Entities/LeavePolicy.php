<?php

namespace App\Entities;
use Doctrine\ORM\Mapping AS ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="TB_LEAVE_POLICY")
 */

class LeavePolicy
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer" , length=11)
     */
    protected $company_id;
    /**
     * @ORM\Id
     * @ORM\Column(type="integer" , length=11)
     */
    protected $leave_policy_id;
    /**
     * @ORM\Column(type="string" , length=100, nullable = true)
     */
    protected $leave_policy_name;
    /**
     * @ORM\Column(type="string" , length=300, nullable = true)
     */
    protected $leave_policy_description;
    /**
     * @ORM\Column(type="integer" , length=11, nullable = true)
     */
    protected $leave_policy_status;
    /**
     * @ORM\Column(type="string" , length=1, nullable = true)
     */
    protected $leave_policy_round_type;
    /**
     * @ORM\Column(type="date" , nullable = true)
     */
    protected $leave_policy_round_date;
    /**
     * @ORM\Column(type="string" , length=1, nullable = true)
     */
    protected $leave_policy_default_flag;
    /**
     * @ORM\Column(type="string" , length=1, nullable = true)
     */
    protected $leave_policy_approve_type;
    /**
     * @ORM\Column(type="integer" , length=11, nullable = true)
     */
    protected $leave_policy_approve_count;
    /**
     * @ORM\Column(type="datetime" )
     */
    protected $created_date;
    /**
     * @ORM\Column(type="integer" , length=11, nullable = true)
     */
    protected $created_by;
    /**
     * @ORM\Column(type="string" , length=30, nullable = true)
     */
    protected $created_by_ip;
    /**
     * @ORM\Column(type="datetime" )
     */
    protected $updated_date;
    /**
     * @ORM\Column(type="integer" , length=11, nullable = true)
     */
    protected $updated_by;
    /**
     * @ORM\Column(type="string" , length=30, nullable = true)
     */
    protected $updated_by_ip;
    /**
     * @ORM\Column(type="datetime" )
     */
    protected $deleted_date;
    /**
     * @ORM\Column(type="integer" , length=11, nullable = true)
     */
    protected $deleted_by;
    /**
     * @ORM\Column(type="string" , length=30, nullable = true)
     */
    protected $deleted_by_ip;

    /**
     * @return mixed
     */
    public function getCompanyId()
    {
        return $this->company_id;
    }

    /**
     * @param mixed $company_id
     */
    public function setCompanyId($company_id)
    {
        $this->company_id = $company_id;
    }

    /**
     * @return mixed
     */
    public function getLeavePolicyId()
    {
        return $this->leave_policy_id;
    }

    /**
     * @param mixed $leave_policy_id
     */
    public function setLeavePolicyId($leave_policy_id)
    {
        $this->leave_policy_id = $leave_policy_id;
    }

    /**
     * @return mixed
     */
    public function getLeavePolicyName()
    {
        return $this->leave_policy_name;
    }

    /**
     * @param mixed $leave_policy_name
     */
    public function setLeavePolicyName($leave_policy_name)
    {
        $this->leave_policy_name = $leave_policy_name;
    }

    /**
     * @return mixed
     */
    public function getLeavePolicyDescription()
    {
        return $this->leave_policy_description;
    }

    /**
     * @param mixed $leave_policy_description
     */
    public function setLeavePolicyDescription($leave_policy_description)
    {
        $this->leave_policy_description = $leave_policy_description;
    }

    /**
     * @return mixed
     */
    public function getLeavePolicyStatus()
    {
        return $this->leave_policy_status;
    }

    /**
     * @param mixed $leave_policy_status
     */
    public function setLeavePolicyStatus($leave_policy_status)
    {
        $this->leave_policy_status = $leave_policy_status;
    }

    /**
     * @return mixed
     */
    public function getLeavePolicyRoundType()
    {
        return $this->leave_policy_round_type;
    }

    /**
     * @param mixed $leave_policy_round_type
     */
    public function setLeavePolicyRoundType($leave_policy_round_type)
    {
        $this->leave_policy_round_type = $leave_policy_round_type;
    }

    /**
     * @return mixed
     */
    public function getLeavePolicyRoundDate()
    {
        return $this->leave_policy_round_date;
    }

    /**
     * @param mixed $leave_policy_round_date
     */
    public function setLeavePolicyRoundDate($leave_policy_round_date)
    {
        $this->leave_policy_round_date = $leave_policy_round_date;
    }

    /**
     * @return mixed
     */
    public function getLeavePolicyDefaultFlag()
    {
        return $this->leave_policy_default_flag;
    }

    /**
     * @param mixed $leave_policy_default_flag
     */
    public function setLeavePolicyDefaultFlag($leave_policy_default_flag)
    {
        $this->leave_policy_default_flag = $leave_policy_default_flag;
    }

    /**
     * @return mixed
     */
    public function getLeavePolicyApproveType()
    {
        return $this->leave_policy_approve_type;
    }

    /**
     * @param mixed $leave_policy_approve_type
     */
    public function setLeavePolicyApproveType($leave_policy_approve_type)
    {
        $this->leave_policy_approve_type = $leave_policy_approve_type;
    }

    /**
     * @return mixed
     */
    public function getLeavePolicyApproveCount()
    {
        return $this->leave_policy_approve_count;
    }

    /**
     * @param mixed $leave_policy_approve_count
     */
    public function setLeavePolicyApproveCount($leave_policy_approve_count)
    {
        $this->leave_policy_approve_count = $leave_policy_approve_count;
    }

    /**
     * @return mixed
     */
    public function getCreatedDate()
    {
        return $this->created_date;
    }

    /**
     * @param mixed $created_date
     */
    public function setCreatedDate($created_date)
    {
        $this->created_date = $created_date;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * @param mixed $created_by
     */
    public function setCreatedBy($created_by)
    {
        $this->created_by = $created_by;
    }

    /**
     * @return mixed
     */
    public function getCreatedByIp()
    {
        return $this->created_by_ip;
    }

    /**
     * @param mixed $created_by_ip
     */
    public function setCreatedByIp($created_by_ip)
    {
        $this->created_by_ip = $created_by_ip;
    }

    /**
     * @return mixed
     */
    public function getUpdatedDate()
    {
        return $this->updated_date;
    }

    /**
     * @param mixed $updated_date
     */
    public function setUpdatedDate($updated_date)
    {
        $this->updated_date = $updated_date;
    }

    /**
     * @return mixed
     */
    public function getUpdatedBy()
    {
        return $this->updated_by;
    }

    /**
     * @param mixed $updated_by
     */
    public function setUpdatedBy($updated_by)
    {
        $this->updated_by = $updated_by;
    }

    /**
     * @return mixed
     */
    public function getUpdatedByIp()
    {
        return $this->updated_by_ip;
    }

    /**
     * @param mixed $updated_by_ip
     */
    public function setUpdatedByIp($updated_by_ip)
    {
        $this->updated_by_ip = $updated_by_ip;
    }

    /**
     * @return mixed
     */
    public function getDeletedDate()
    {
        return $this->deleted_date;
    }

    /**
     * @param mixed $deleted_date
     */
    public function setDeletedDate($deleted_date)
    {
        $this->deleted_date = $deleted_date;
    }

    /**
     * @return mixed
     */
    public function getDeletedBy()
    {
        return $this->deleted_by;
    }

    /**
     * @param mixed $deleted_by
     */
    public function setDeletedBy($deleted_by)
    {
        $this->deleted_by = $deleted_by;
    }

    /**
     * @return mixed
     */
    public function getDeletedByIp()
    {
        return $this->deleted_by_ip;
    }

    /**
     * @param mixed $deleted_by_ip
     */
    public function setDeletedByIp($deleted_by_ip)
    {
        $this->deleted_by_ip = $deleted_by_ip;
    }

}
