<?php
namespace App\Entities;
use Doctrine\ORM\Mapping AS ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="TB_PAYRULE_DETAIL")
 */

class PayruleDetail
{


    /**
     * @ORM\Id

     * @ORM\Column(type="integer" , length=11 )
     */
    protected $company_id;
    /**
     * @ORM\Id

     * @ORM\Column(type="integer" , length=11 )
     */
    protected $payrule_detail_id;
    /**

     * @ORM\Column(type="date"  )
     */
    protected $close_date;
    /**

     * @ORM\Column(type="date" )
     */
    protected $payment_date;
    /**

     * @ORM\Column(type="integer" , length=11 )
     */
    protected $payrule_detail_status;
    /**

     * @ORM\Column(type="datetime" , nullable = true)
     */
    protected $created_date;
    /**

     * @ORM\Column(type="integer" , length=11 , nullable = true)
     */
    protected $created_by;
    /**

     * @ORM\Column(type="string" , length=30, nullable = true)
     */
    protected $created_by_ip;
    /**

     * @ORM\Column(type="datetime", nullable = true)
     */
    protected $updated_date;
    /**

     * @ORM\Column(type="integer" , length=11 , nullable = true)
     */
    protected $updated_by;
    /**

     * @ORM\Column(type="string" , length=30 , nullable = true)
     */
    protected $updated_by_ip;
    /**

     * @ORM\Column(type="datetime" , nullable = true )
     */
    protected $deleted_date;
    /**

     * @ORM\Column(type="integer" , length=11 , nullable = true)
     */
    protected $deleted_by;
    /**

     * @ORM\Column(type="string" , length=30 , nullable = true)
     */
    protected $deleted_by_ip;

    /**
     * @return mixed
     */
    public function getCompanyId()
    {
        return $this->company_id;
    }

    /**
     * @param mixed $company_id
     */
    public function setCompanyId($company_id)
    {
        $this->company_id = $company_id;
    }

    /**
     * @return mixed
     */
    public function getPayruleDetailId()
    {
        return $this->payrule_detail_id;
    }

    /**
     * @param mixed $payrule_detail_id
     */
    public function setPayruleDetailId($payrule_detail_id)
    {
        $this->payrule_detail_id = $payrule_detail_id;
    }

    /**
     * @return mixed
     */
    public function getCloseDate()
    {
        return $this->close_date;
    }

    /**
     * @param mixed $close_date
     */
    public function setCloseDate($close_date)
    {
        $this->close_date = $close_date;
    }

    /**
     * @return mixed
     */
    public function getPaymentDate()
    {
        return $this->payment_date;
    }

    /**
     * @param mixed $payment_date
     */
    public function setPaymentDate($payment_date)
    {
        $this->payment_date = $payment_date;
    }

    /**
     * @return mixed
     */
    public function getPayruleDetailStatus()
    {
        return $this->payrule_detail_status;
    }

    /**
     * @param mixed $payrule_detail_status
     */
    public function setPayruleDetailStatus($payrule_detail_status)
    {
        $this->payrule_detail_status = $payrule_detail_status;
    }

    /**
     * @return mixed
     */
    public function getCreatedDate()
    {
        return $this->created_date;
    }

    /**
     * @param mixed $created_date
     */
    public function setCreatedDate($created_date)
    {
        $this->created_date = $created_date;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * @param mixed $created_by
     */
    public function setCreatedBy($created_by)
    {
        $this->created_by = $created_by;
    }

    /**
     * @return mixed
     */
    public function getCreatedByIp()
    {
        return $this->created_by_ip;
    }

    /**
     * @param mixed $created_by_ip
     */
    public function setCreatedByIp($created_by_ip)
    {
        $this->created_by_ip = $created_by_ip;
    }

    /**
     * @return mixed
     */
    public function getUpdatedDate()
    {
        return $this->updated_date;
    }

    /**
     * @param mixed $updated_date
     */
    public function setUpdatedDate($updated_date)
    {
        $this->updated_date = $updated_date;
    }

    /**
     * @return mixed
     */
    public function getUpdatedBy()
    {
        return $this->updated_by;
    }

    /**
     * @param mixed $updated_by
     */
    public function setUpdatedBy($updated_by)
    {
        $this->updated_by = $updated_by;
    }

    /**
     * @return mixed
     */
    public function getUpdatedByIp()
    {
        return $this->updated_by_ip;
    }

    /**
     * @param mixed $updated_by_ip
     */
    public function setUpdatedByIp($updated_by_ip)
    {
        $this->updated_by_ip = $updated_by_ip;
    }

    /**
     * @return mixed
     */
    public function getDeletedDate()
    {
        return $this->deleted_date;
    }

    /**
     * @param mixed $deleted_date
     */
    public function setDeletedDate($deleted_date)
    {
        $this->deleted_date = $deleted_date;
    }

    /**
     * @return mixed
     */
    public function getDeletedBy()
    {
        return $this->deleted_by;
    }

    /**
     * @param mixed $deleted_by
     */
    public function setDeletedBy($deleted_by)
    {
        $this->deleted_by = $deleted_by;
    }

    /**
     * @return mixed
     */
    public function getDeletedByIp()
    {
        return $this->deleted_by_ip;
    }

    /**
     * @param mixed $deleted_by_ip
     */
    public function setDeletedByIp($deleted_by_ip)
    {
        $this->deleted_by_ip = $deleted_by_ip;
    }


}