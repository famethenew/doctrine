<?php
namespace App\Entities;
use Doctrine\ORM\Mapping AS ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="TB_LEAVE_DETAIL")
 */

class LeaveDetail
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer" , length=11)
     */
    protected $company_id;
    /**
     * @ORM\Id
     * @ORM\Column(type="integer" , length=11)
     */
    protected $leave_detail_id;
    /**
     * @ORM\Column(type="string" , length=100, nullable = true)
     */
    protected  $leave_detail_name;
    /**
     * @ORM\Column(type="string" , length=1, nullable = true)
     */
    protected  $leave_detail_type;
    /**
     * @ORM\Column(type="string" , length=1, nullable = true)
     */
    protected  $leave_detail_main_flag;
    /**
     * @ORM\Column(type="integer" , length=11, nullable = true)
     */
    protected  $leave_detail_status;
    /**
     * @ORM\Column(type="integer" , length=11, nullable = true)
     */
    protected  $leave_detail_compensate;
    /**
     * @ORM\Column(type="datetime" )
     */
    protected  $created_date;
    /**
     * @ORM\Column(type="integer" , length=11, nullable = true)
     */
    protected  $created_by;
    /**
     * @ORM\Column(type="string" , length=30, nullable = true)
     */
    protected  $created_by_ip;
    /**
     * @ORM\Column(type="datetime" )
     */
    protected  $updated_date;
    /**
     * @ORM\Column(type="integer" , length=11, nullable = true)
     */
    protected  $updated_by;
    /**
     * @ORM\Column(type="string" , length=30, nullable = true)
     */
    protected  $updated_by_ip;
    /**
     * @ORM\Column(type="datetime" )
     */
    protected  $deleted_date;
    /**
     * @ORM\Column(type="integer" , length=11, nullable = true)
     */
    protected  $deleted_by;
    /**
     * @ORM\Column(type="string" , length=30, nullable = true)
     */
    protected  $deleted_by_ip;

    /**
     * @return mixed
     */
    public function getCompanyId()
    {
        return $this->company_id;
    }

    /**
     * @param mixed $company_id
     */
    public function setCompanyId($company_id)
    {
        $this->company_id = $company_id;
    }

    /**
     * @return mixed
     */
    public function getLeaveDetailId()
    {
        return $this->leave_detail_id;
    }

    /**
     * @param mixed $leave_detail_id
     */
    public function setLeaveDetailId($leave_detail_id)
    {
        $this->leave_detail_id = $leave_detail_id;
    }

    /**
     * @return mixed
     */
    public function getLeaveDetailName()
    {
        return $this->leave_detail_name;
    }

    /**
     * @param mixed $leave_detail_name
     */
    public function setLeaveDetailName($leave_detail_name)
    {
        $this->leave_detail_name = $leave_detail_name;
    }

    /**
     * @return mixed
     */
    public function getLeaveDetailType()
    {
        return $this->leave_detail_type;
    }

    /**
     * @param mixed $leave_detail_type
     */
    public function setLeaveDetailType($leave_detail_type)
    {
        $this->leave_detail_type = $leave_detail_type;
    }

    /**
     * @return mixed
     */
    public function getLeaveDetailMainFlag()
    {
        return $this->leave_detail_main_flag;
    }

    /**
     * @param mixed $leave_detail_main_flag
     */
    public function setLeaveDetailMainFlag($leave_detail_main_flag)
    {
        $this->leave_detail_main_flag = $leave_detail_main_flag;
    }

    /**
     * @return mixed
     */
    public function getLeaveDetailStatus()
    {
        return $this->leave_detail_status;
    }

    /**
     * @param mixed $leave_detail_status
     */
    public function setLeaveDetailStatus($leave_detail_status)
    {
        $this->leave_detail_status = $leave_detail_status;
    }

    /**
     * @return mixed
     */
    public function getLeaveDetailCompensate()
    {
        return $this->leave_detail_compensate;
    }

    /**
     * @param mixed $leave_detail_compensate
     */
    public function setLeaveDetailCompensate($leave_detail_compensate)
    {
        $this->leave_detail_compensate = $leave_detail_compensate;
    }

    /**
     * @return mixed
     */
    public function getCreatedDate()
    {
        return $this->created_date;
    }

    /**
     * @param mixed $created_date
     */
    public function setCreatedDate($created_date)
    {
        $this->created_date = $created_date;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * @param mixed $created_by
     */
    public function setCreatedBy($created_by)
    {
        $this->created_by = $created_by;
    }

    /**
     * @return mixed
     */
    public function getCreatedByIp()
    {
        return $this->created_by_ip;
    }

    /**
     * @param mixed $created_by_ip
     */
    public function setCreatedByIp($created_by_ip)
    {
        $this->created_by_ip = $created_by_ip;
    }

    /**
     * @return mixed
     */
    public function getUpdatedDate()
    {
        return $this->updated_date;
    }

    /**
     * @param mixed $updated_date
     */
    public function setUpdatedDate($updated_date)
    {
        $this->updated_date = $updated_date;
    }

    /**
     * @return mixed
     */
    public function getUpdatedBy()
    {
        return $this->updated_by;
    }

    /**
     * @param mixed $updated_by
     */
    public function setUpdatedBy($updated_by)
    {
        $this->updated_by = $updated_by;
    }

    /**
     * @return mixed
     */
    public function getUpdatedByIp()
    {
        return $this->updated_by_ip;
    }

    /**
     * @param mixed $updated_by_ip
     */
    public function setUpdatedByIp($updated_by_ip)
    {
        $this->updated_by_ip = $updated_by_ip;
    }

    /**
     * @return mixed
     */
    public function getDeletedDate()
    {
        return $this->deleted_date;
    }

    /**
     * @param mixed $deleted_date
     */
    public function setDeletedDate($deleted_date)
    {
        $this->deleted_date = $deleted_date;
    }

    /**
     * @return mixed
     */
    public function getDeletedBy()
    {
        return $this->deleted_by;
    }

    /**
     * @param mixed $deleted_by
     */
    public function setDeletedBy($deleted_by)
    {
        $this->deleted_by = $deleted_by;
    }

    /**
     * @return mixed
     */
    public function getDeletedByIp()
    {
        return $this->deleted_by_ip;
    }

    /**
     * @param mixed $deleted_by_ip
     */
    public function setDeletedByIp($deleted_by_ip)
    {
        $this->deleted_by_ip = $deleted_by_ip;
    }

}