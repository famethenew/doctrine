<?php
namespace App\Entities;
use Doctrine\ORM\Mapping AS ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="TB_PAYRULE_POLICY")
 */

class PayrulePolicy
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer" , length=11)
     */
    protected $company_id;
    /**
     * @ORM\Id
     * @ORM\Column(type="integer" , length=11)
     */
    protected $payrule_policy_id;
    /**
     * @ORM\Column(type="string" , length=100, nullable = true)
     */
    protected  $payrule_policy_name;
    /**
     * @ORM\Column(type="string" , length=300, nullable = true)
     */
    protected  $payrule_policy_description;
    /**
     * @ORM\Column(type="integer" , length=11, nullable = true)
     */
    protected  $payrule_policy_status;
    /**
     * @ORM\Column(type="string" , length=1, nullable = true)
     */
    protected  $payrule_policy_default_flag;
    /**
     * @ORM\Column(type="date" )
     */
    protected  $begin_close_date;
    /**
     * @ORM\Column(type="date" )
     */
    protected  $begin_payment_date;
    /**
     * @ORM\Column(type="date" )
     */
    protected  $created_date;
    /**
     * @ORM\Column(type="integer" , length=11, nullable = true)
     */
    protected  $created_by;
    /**
     * @ORM\Column(type="string" , length=30, nullable = true)
     */
    protected  $created_by_ip;
    /**
     * @ORM\Column(type="date" )
     */
    protected  $updated_date;
    /**
     * @ORM\Column(type="integer" , length=11, nullable = true)
     */
    protected  $updated_by;
    /**
     * @ORM\Column(type="string" , length=30, nullable = true)
     */
    protected  $updated_by_ip;
    /**
     * @ORM\Column(type="datetime" )
     */
    protected  $deleted_date;
    /**
     * @ORM\Column(type="integer" , length=11, nullable = true)
     */
    protected  $deleted_by;
    /**
     * @ORM\Column(type="string" , length=30, nullable = true)
     */
    protected  $deleted_by_ip;

    /**
     * @return mixed
     */
    public function getCompanyId()
    {
        return $this->company_id;
    }

    /**
     * @param mixed $company_id
     */
    public function setCompanyId($company_id)
    {
        $this->company_id = $company_id;
    }

    /**
     * @return mixed
     */
    public function getPayrulePolicyId()
    {
        return $this->payrule_policy_id;
    }

    /**
     * @param mixed $payrule_policy_id
     */
    public function setPayrulePolicyId($payrule_policy_id)
    {
        $this->payrule_policy_id = $payrule_policy_id;
    }

    /**
     * @return mixed
     */
    public function getPayrulePolicyName()
    {
        return $this->payrule_policy_name;
    }

    /**
     * @param mixed $payrule_policy_name
     */
    public function setPayrulePolicyName($payrule_policy_name)
    {
        $this->payrule_policy_name = $payrule_policy_name;
    }

    /**
     * @return mixed
     */
    public function getPayrulePolicyDescription()
    {
        return $this->payrule_policy_description;
    }

    /**
     * @param mixed $payrule_policy_description
     */
    public function setPayrulePolicyDescription($payrule_policy_description)
    {
        $this->payrule_policy_description = $payrule_policy_description;
    }

    /**
     * @return mixed
     */
    public function getPayrulePolicyStatus()
    {
        return $this->payrule_policy_status;
    }

    /**
     * @param mixed $payrule_policy_status
     */
    public function setPayrulePolicyStatus($payrule_policy_status)
    {
        $this->payrule_policy_status = $payrule_policy_status;
    }

    /**
     * @return mixed
     */
    public function getPayrulePolicyDefaultFlag()
    {
        return $this->payrule_policy_default_flag;
    }

    /**
     * @param mixed $payrule_policy_default_flag
     */
    public function setPayrulePolicyDefaultFlag($payrule_policy_default_flag)
    {
        $this->payrule_policy_default_flag = $payrule_policy_default_flag;
    }

    /**
     * @return mixed
     */
    public function getBeginCloseDate()
    {
        return $this->begin_close_date;
    }

    /**
     * @param mixed $begin_close_date
     */
    public function setBeginCloseDate($begin_close_date)
    {
        $this->begin_close_date = $begin_close_date;
    }

    /**
     * @return mixed
     */
    public function getBeginPaymentDate()
    {
        return $this->begin_payment_date;
    }

    /**
     * @param mixed $begin_payment_date
     */
    public function setBeginPaymentDate($begin_payment_date)
    {
        $this->begin_payment_date = $begin_payment_date;
    }

    /**
     * @return mixed
     */
    public function getCreatedDate()
    {
        return $this->created_date;
    }

    /**
     * @param mixed $created_date
     */
    public function setCreatedDate($created_date)
    {
        $this->created_date = $created_date;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * @param mixed $created_by
     */
    public function setCreatedBy($created_by)
    {
        $this->created_by = $created_by;
    }

    /**
     * @return mixed
     */
    public function getCreatedByIp()
    {
        return $this->created_by_ip;
    }

    /**
     * @param mixed $created_by_ip
     */
    public function setCreatedByIp($created_by_ip)
    {
        $this->created_by_ip = $created_by_ip;
    }

    /**
     * @return mixed
     */
    public function getUpdatedDate()
    {
        return $this->updated_date;
    }

    /**
     * @param mixed $updated_date
     */
    public function setUpdatedDate($updated_date)
    {
        $this->updated_date = $updated_date;
    }

    /**
     * @return mixed
     */
    public function getUpdatedBy()
    {
        return $this->updated_by;
    }

    /**
     * @param mixed $updated_by
     */
    public function setUpdatedBy($updated_by)
    {
        $this->updated_by = $updated_by;
    }

    /**
     * @return mixed
     */
    public function getUpdatedByIp()
    {
        return $this->updated_by_ip;
    }

    /**
     * @param mixed $updated_by_ip
     */
    public function setUpdatedByIp($updated_by_ip)
    {
        $this->updated_by_ip = $updated_by_ip;
    }

    /**
     * @return mixed
     */
    public function getDeletedDate()
    {
        return $this->deleted_date;
    }

    /**
     * @param mixed $deleted_date
     */
    public function setDeletedDate($deleted_date)
    {
        $this->deleted_date = $deleted_date;
    }

    /**
     * @return mixed
     */
    public function getDeletedBy()
    {
        return $this->deleted_by;
    }

    /**
     * @param mixed $deleted_by
     */
    public function setDeletedBy($deleted_by)
    {
        $this->deleted_by = $deleted_by;
    }

    /**
     * @return mixed
     */
    public function getDeletedByIp()
    {
        return $this->deleted_by_ip;
    }

    /**
     * @param mixed $deleted_by_ip
     */
    public function setDeletedByIp($deleted_by_ip)
    {
        $this->deleted_by_ip = $deleted_by_ip;
    }

}
