<?php
namespace App\Entities;
use Doctrine\ORM\Mapping AS ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="TB_OVERTIME_REQUEST")
 */

class OvertimeRequest

{


    /**
     * @ORM\Id
     * @ORM\Column(type="integer" , length=11 )
     */
    protected $company_id;
    /**
     * @ORM\Id
     * @ORM\Column(type="integer" , length=11 )
     */
    protected $profile_id;
    /**
     * @ORM\Column(type="integer" , length=11 , nullable = true )
     */
    protected $overtime_request_id;
    /**
     * @ORM\Column(type="date" , nullable = true )
     */
    protected $overtime_request_work_date;
    /**
     * @ORM\Column(type="string" , length=30 , nullable = true )
     */
    protected $overtime_request_no;
    /**
     * @ORM\Column(type="string" , length=300 , nullable = true )
     */
    protected $overtime_request_description;
    /**
     * @ORM\Column(type="integer" , length=11 , nullable = true )
     */
    protected $overtime_request_minute;
    /**
     * @ORM\Column(type="float" , nullable = true )
     */
    protected $overtime_request_rate;
    /**
     * @ORM\Column(type="integer" , length=11 , nullable = true )
     */
    protected $overtime_request_approver_count;
    /**
     * @ORM\Column(type="integer" , length=11 , nullable = true )
     */
    protected $overtime_request_total_approver;
    /**
     * @ORM\Column(type="string" , length=1 , nullable = true )
     */
    protected $overtime_requested_status;
    /**
     * @ORM\Column(type="datetime"  , nullable = true )
     */
    protected $overtime_requested_date;
    /**
     * @ORM\Column(type="integer" , length=11 , nullable = true )
     */
    protected $overtime_requested_by;
    /**
     * @ORM\Column(type="string" , length=30 , nullable = true )
     */
    protected $overtime_requested_by_ip;
    /**
     * @ORM\Column(type="datetime"  , nullable = true )
     */
    protected $overtime_rejected_date;
    /**
     * @ORM\Column(type="integer" , length=11 , nullable = true )
     */
    protected $overtime_rejected_by;
    /**
     * @ORM\Column(type="string" , length=30 , nullable = true )
     */
    protected $overtime_rejected_by_ip;

    /**
     * @return mixed
     */
    public function getCompanyId()
    {
        return $this->company_id;
    }

    /**
     * @param mixed $company_id
     */
    public function setCompanyId($company_id)
    {
        $this->company_id = $company_id;
    }

    /**
     * @return mixed
     */
    public function getProfileId()
    {
        return $this->profile_id;
    }

    /**
     * @param mixed $profile_id
     */
    public function setProfileId($profile_id)
    {
        $this->profile_id = $profile_id;
    }

    /**
     * @return mixed
     */
    public function getOvertimeRequestId()
    {
        return $this->overtime_request_id;
    }

    /**
     * @param mixed $overtime_request_id
     */
    public function setOvertimeRequestId($overtime_request_id)
    {
        $this->overtime_request_id = $overtime_request_id;
    }

    /**
     * @return mixed
     */
    public function getOvertimeRequestWorkDate()
    {
        return $this->overtime_request_work_date;
    }

    /**
     * @param mixed $overtime_request_work_date
     */
    public function setOvertimeRequestWorkDate($overtime_request_work_date)
    {
        $this->overtime_request_work_date = $overtime_request_work_date;
    }

    /**
     * @return mixed
     */
    public function getOvertimeRequestNo()
    {
        return $this->overtime_request_no;
    }

    /**
     * @param mixed $overtime_request_no
     */
    public function setOvertimeRequestNo($overtime_request_no)
    {
        $this->overtime_request_no = $overtime_request_no;
    }

    /**
     * @return mixed
     */
    public function getOvertimeRequestDescription()
    {
        return $this->overtime_request_description;
    }

    /**
     * @param mixed $overtime_request_description
     */
    public function setOvertimeRequestDescription($overtime_request_description)
    {
        $this->overtime_request_description = $overtime_request_description;
    }

    /**
     * @return mixed
     */
    public function getOvertimeRequestMinute()
    {
        return $this->overtime_request_minute;
    }

    /**
     * @param mixed $overtime_request_minute
     */
    public function setOvertimeRequestMinute($overtime_request_minute)
    {
        $this->overtime_request_minute = $overtime_request_minute;
    }

    /**
     * @return mixed
     */
    public function getOvertimeRequestRate()
    {
        return $this->overtime_request_rate;
    }

    /**
     * @param mixed $overtime_request_rate
     */
    public function setOvertimeRequestRate($overtime_request_rate)
    {
        $this->overtime_request_rate = $overtime_request_rate;
    }

    /**
     * @return mixed
     */
    public function getOvertimeRequestApproverCount()
    {
        return $this->overtime_request_approver_count;
    }

    /**
     * @param mixed $overtime_request_approver_count
     */
    public function setOvertimeRequestApproverCount($overtime_request_approver_count)
    {
        $this->overtime_request_approver_count = $overtime_request_approver_count;
    }

    /**
     * @return mixed
     */
    public function getOvertimeRequestTotalApprover()
    {
        return $this->overtime_request_total_approver;
    }

    /**
     * @param mixed $overtime_request_total_approver
     */
    public function setOvertimeRequestTotalApprover($overtime_request_total_approver)
    {
        $this->overtime_request_total_approver = $overtime_request_total_approver;
    }

    /**
     * @return mixed
     */
    public function getOvertimeRequestedStatus()
    {
        return $this->overtime_requested_status;
    }

    /**
     * @param mixed $overtime_requested_status
     */
    public function setOvertimeRequestedStatus($overtime_requested_status)
    {
        $this->overtime_requested_status = $overtime_requested_status;
    }

    /**
     * @return mixed
     */
    public function getOvertimeRequestedDate()
    {
        return $this->overtime_requested_date;
    }

    /**
     * @param mixed $overtime_requested_date
     */
    public function setOvertimeRequestedDate($overtime_requested_date)
    {
        $this->overtime_requested_date = $overtime_requested_date;
    }

    /**
     * @return mixed
     */
    public function getOvertimeRequestedBy()
    {
        return $this->overtime_requested_by;
    }

    /**
     * @param mixed $overtime_requested_by
     */
    public function setOvertimeRequestedBy($overtime_requested_by)
    {
        $this->overtime_requested_by = $overtime_requested_by;
    }

    /**
     * @return mixed
     */
    public function getOvertimeRequestedByIp()
    {
        return $this->overtime_requested_by_ip;
    }

    /**
     * @param mixed $overtime_requested_by_ip
     */
    public function setOvertimeRequestedByIp($overtime_requested_by_ip)
    {
        $this->overtime_requested_by_ip = $overtime_requested_by_ip;
    }

    /**
     * @return mixed
     */
    public function getOvertimeRejectedDate()
    {
        return $this->overtime_rejected_date;
    }

    /**
     * @param mixed $overtime_rejected_date
     */
    public function setOvertimeRejectedDate($overtime_rejected_date)
    {
        $this->overtime_rejected_date = $overtime_rejected_date;
    }

    /**
     * @return mixed
     */
    public function getOvertimeRejectedBy()
    {
        return $this->overtime_rejected_by;
    }

    /**
     * @param mixed $overtime_rejected_by
     */
    public function setOvertimeRejectedBy($overtime_rejected_by)
    {
        $this->overtime_rejected_by = $overtime_rejected_by;
    }

    /**
     * @return mixed
     */
    public function getOvertimeRejectedByIp()
    {
        return $this->overtime_rejected_by_ip;
    }

    /**
     * @param mixed $overtime_rejected_by_ip
     */
    public function setOvertimeRejectedByIp($overtime_rejected_by_ip)
    {
        $this->overtime_rejected_by_ip = $overtime_rejected_by_ip;
    }


}