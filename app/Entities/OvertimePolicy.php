<?php
namespace App\Entities;
use Doctrine\ORM\Mapping AS ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="TB_OVERTIME_POLICY")
 */

class OvertimePolicy

{


    /**
     * @ORM\Id
     * @ORM\Column(type="integer" , length=11 )
     */
    protected $company_id;
    /**
     * @ORM\Column(type="integer" , length=11, nullable = true )
     */
    protected $overtime_policy_id;
    /**
     * @ORM\Column(type="string", length=100, nullable = true )
     */
    protected $overtime_policy_name;
    /**
     * @ORM\Column(type="string" , length=300, nullable = true )
     */
    protected $overtime_policy_description;
    /**
     * @ORM\Column(type="float" , nullable = true )
     */
    protected $overtime_policy_work_ot;
    /**
     * @ORM\Column(type="float" , nullable = true  )
     */
    protected $overtime_policy_work_holiday;
    /**
     * @ORM\Column(type="float" , nullable = true )
     */
    protected $overtime_policy_work_holiday_ot;
    /**
     * @ORM\Column(type="integer" , length=11 , nullable = true )
     */
    protected $overtime_policy_min_minute;
    /**
     * @ORM\Column(type="integer" , length=11 , nullable = true )
     */
    protected $overtime_policy_max_minute;
    /**
     * @ORM\Column(type="integer" , length=11 , nullable = true )
     */
    protected $overtime_policy_minute_time_range;
    /**
     * @ORM\Column(type="string" , length=1 , nullable = true )
     */
    protected $overtime_policy_minute_time_full;
    /**
     * @ORM\Column(type="integer" , length=11 , nullable = true )
     */
    protected $overtime_policy_break_over;
    /**
     * @ORM\Column(type="integer" , length=11, nullable = true  )
     */
    protected $overtime_policy_break_minute;
    /**
     * @ORM\Column(type="string" , length=1 , nullable = true )
     */
    protected $overtime_policy_approve_auto;
    /**
     * @ORM\Column(type="integer" , length=11, nullable = true  )
     */
    protected $overtime_policy_approve_auto_minute;
    /**
     * @ORM\Column(type="string" , length=1 , nullable = true )
     */
    protected $overtime_policy_before_work;
    /**
     * @ORM\Column(type="string" , length=1 , nullable = true )
     */
    protected $overtime_policy_after_work;
    /**
     * @ORM\Column(type="integer" , length=11 , nullable = true )
     */
    protected $overtime_policy_status;
    /**
     * @ORM\Column(type="string" , length=1 , nullable = true )
     */
    protected $overtime_policy_usage_flag;
    /**
     * @ORM\Column(type="string" , length=1 , nullable = true )
     */
    protected $overtime_policy_approve_type;
    /**
     * @ORM\Column(type="integer" , length=11 , nullable = true )
     */
    protected $overtime_policy_approve_count;
    /**
     * @ORM\Column(type="datetime" , nullable = true  )
     */
    protected $created_date;
    /**
     * @ORM\Column(type="integer" , length=11 , nullable = true )
     */
    protected $created_by;
    /**
     * @ORM\Column(type="string" , length=30, nullable = true  )
     */
    protected $created_by_ip;
    /**
     * @ORM\Column(type="datetime" , nullable = true  )
     */
    protected $updated_date;
    /**
     * @ORM\Column(type="integer" , length=11 , nullable = true )
     */
    protected $updated_by;
    /**
     * @ORM\Column(type="string" , length=30 , nullable = true )
     */
    protected $updated_by_ip;
    /**
     * @ORM\Column(type="datetime" , nullable = true  )
     */
    protected $deleted_date;
    /**
     * @ORM\Column(type="integer" , length=11 , nullable = true )
     */
    protected $deleted_by;
    /**
     * @ORM\Column(type="string" , length=30 , nullable = true )
     */
    protected $deleted_by_ip;

    /**
     * @return mixed
     */
    public function getCompanyId()
    {
        return $this->company_id;
    }

    /**
     * @param mixed $company_id
     */
    public function setCompanyId($company_id)
    {
        $this->company_id = $company_id;
    }

    /**
     * @return mixed
     */
    public function getOvertimePolicyId()
    {
        return $this->overtime_policy_id;
    }

    /**
     * @param mixed $overtime_policy_id
     */
    public function setOvertimePolicyId($overtime_policy_id)
    {
        $this->overtime_policy_id = $overtime_policy_id;
    }

    /**
     * @return mixed
     */
    public function getOvertimePolicyName()
    {
        return $this->overtime_policy_name;
    }

    /**
     * @param mixed $overtime_policy_name
     */
    public function setOvertimePolicyName($overtime_policy_name)
    {
        $this->overtime_policy_name = $overtime_policy_name;
    }

    /**
     * @return mixed
     */
    public function getOvertimePolicyDescription()
    {
        return $this->overtime_policy_description;
    }

    /**
     * @param mixed $overtime_policy_description
     */
    public function setOvertimePolicyDescription($overtime_policy_description)
    {
        $this->overtime_policy_description = $overtime_policy_description;
    }

    /**
     * @return mixed
     */
    public function getOvertimePolicyWorkOt()
    {
        return $this->overtime_policy_work_ot;
    }

    /**
     * @param mixed $overtime_policy_work_ot
     */
    public function setOvertimePolicyWorkOt($overtime_policy_work_ot)
    {
        $this->overtime_policy_work_ot = $overtime_policy_work_ot;
    }

    /**
     * @return mixed
     */
    public function getOvertimePolicyWorkHoliday()
    {
        return $this->overtime_policy_work_holiday;
    }

    /**
     * @param mixed $overtime_policy_work_holiday
     */
    public function setOvertimePolicyWorkHoliday($overtime_policy_work_holiday)
    {
        $this->overtime_policy_work_holiday = $overtime_policy_work_holiday;
    }

    /**
     * @return mixed
     */
    public function getOvertimePolicyWorkHolidayOt()
    {
        return $this->overtime_policy_work_holiday_ot;
    }

    /**
     * @param mixed $overtime_policy_work_holiday_ot
     */
    public function setOvertimePolicyWorkHolidayOt($overtime_policy_work_holiday_ot)
    {
        $this->overtime_policy_work_holiday_ot = $overtime_policy_work_holiday_ot;
    }

    /**
     * @return mixed
     */
    public function getOvertimePolicyMinMinute()
    {
        return $this->overtime_policy_min_minute;
    }

    /**
     * @param mixed $overtime_policy_min_minute
     */
    public function setOvertimePolicyMinMinute($overtime_policy_min_minute)
    {
        $this->overtime_policy_min_minute = $overtime_policy_min_minute;
    }

    /**
     * @return mixed
     */
    public function getOvertimePolicyMaxMinute()
    {
        return $this->overtime_policy_max_minute;
    }

    /**
     * @param mixed $overtime_policy_max_minute
     */
    public function setOvertimePolicyMaxMinute($overtime_policy_max_minute)
    {
        $this->overtime_policy_max_minute = $overtime_policy_max_minute;
    }

    /**
     * @return mixed
     */
    public function getOvertimePolicyMinuteTimeRange()
    {
        return $this->overtime_policy_minute_time_range;
    }

    /**
     * @param mixed $overtime_policy_minute_time_range
     */
    public function setOvertimePolicyMinuteTimeRange($overtime_policy_minute_time_range)
    {
        $this->overtime_policy_minute_time_range = $overtime_policy_minute_time_range;
    }

    /**
     * @return mixed
     */
    public function getOvertimePolicyMinuteTimeFull()
    {
        return $this->overtime_policy_minute_time_full;
    }

    /**
     * @param mixed $overtime_policy_minute_time_full
     */
    public function setOvertimePolicyMinuteTimeFull($overtime_policy_minute_time_full)
    {
        $this->overtime_policy_minute_time_full = $overtime_policy_minute_time_full;
    }

    /**
     * @return mixed
     */
    public function getOvertimePolicyBreakOver()
    {
        return $this->overtime_policy_break_over;
    }

    /**
     * @param mixed $overtime_policy_break_over
     */
    public function setOvertimePolicyBreakOver($overtime_policy_break_over)
    {
        $this->overtime_policy_break_over = $overtime_policy_break_over;
    }

    /**
     * @return mixed
     */
    public function getOvertimePolicyBreakMinute()
    {
        return $this->overtime_policy_break_minute;
    }

    /**
     * @param mixed $overtime_policy_break_minute
     */
    public function setOvertimePolicyBreakMinute($overtime_policy_break_minute)
    {
        $this->overtime_policy_break_minute = $overtime_policy_break_minute;
    }

    /**
     * @return mixed
     */
    public function getOvertimePolicyApproveAuto()
    {
        return $this->overtime_policy_approve_auto;
    }

    /**
     * @param mixed $overtime_policy_approve_auto
     */
    public function setOvertimePolicyApproveAuto($overtime_policy_approve_auto)
    {
        $this->overtime_policy_approve_auto = $overtime_policy_approve_auto;
    }

    /**
     * @return mixed
     */
    public function getOvertimePolicyApproveAutoMinute()
    {
        return $this->overtime_policy_approve_auto_minute;
    }

    /**
     * @param mixed $overtime_policy_approve_auto_minute
     */
    public function setOvertimePolicyApproveAutoMinute($overtime_policy_approve_auto_minute)
    {
        $this->overtime_policy_approve_auto_minute = $overtime_policy_approve_auto_minute;
    }

    /**
     * @return mixed
     */
    public function getOvertimePolicyBeforeWork()
    {
        return $this->overtime_policy_before_work;
    }

    /**
     * @param mixed $overtime_policy_before_work
     */
    public function setOvertimePolicyBeforeWork($overtime_policy_before_work)
    {
        $this->overtime_policy_before_work = $overtime_policy_before_work;
    }

    /**
     * @return mixed
     */
    public function getOvertimePolicyAfterWork()
    {
        return $this->overtime_policy_after_work;
    }

    /**
     * @param mixed $overtime_policy_after_work
     */
    public function setOvertimePolicyAfterWork($overtime_policy_after_work)
    {
        $this->overtime_policy_after_work = $overtime_policy_after_work;
    }

    /**
     * @return mixed
     */
    public function getOvertimePolicyStatus()
    {
        return $this->overtime_policy_status;
    }

    /**
     * @param mixed $overtime_policy_status
     */
    public function setOvertimePolicyStatus($overtime_policy_status)
    {
        $this->overtime_policy_status = $overtime_policy_status;
    }

    /**
     * @return mixed
     */
    public function getOvertimePolicyUsageFlag()
    {
        return $this->overtime_policy_usage_flag;
    }

    /**
     * @param mixed $overtime_policy_usage_flag
     */
    public function setOvertimePolicyUsageFlag($overtime_policy_usage_flag)
    {
        $this->overtime_policy_usage_flag = $overtime_policy_usage_flag;
    }

    /**
     * @return mixed
     */
    public function getOvertimePolicyApproveType()
    {
        return $this->overtime_policy_approve_type;
    }

    /**
     * @param mixed $overtime_policy_approve_type
     */
    public function setOvertimePolicyApproveType($overtime_policy_approve_type)
    {
        $this->overtime_policy_approve_type = $overtime_policy_approve_type;
    }

    /**
     * @return mixed
     */
    public function getOvertimePolicyApproveCount()
    {
        return $this->overtime_policy_approve_count;
    }

    /**
     * @param mixed $overtime_policy_approve_count
     */
    public function setOvertimePolicyApproveCount($overtime_policy_approve_count)
    {
        $this->overtime_policy_approve_count = $overtime_policy_approve_count;
    }

    /**
     * @return mixed
     */
    public function getCreatedDate()
    {
        return $this->created_date;
    }

    /**
     * @param mixed $created_date
     */
    public function setCreatedDate($created_date)
    {
        $this->created_date = $created_date;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * @param mixed $created_by
     */
    public function setCreatedBy($created_by)
    {
        $this->created_by = $created_by;
    }

    /**
     * @return mixed
     */
    public function getCreatedByIp()
    {
        return $this->created_by_ip;
    }

    /**
     * @param mixed $created_by_ip
     */
    public function setCreatedByIp($created_by_ip)
    {
        $this->created_by_ip = $created_by_ip;
    }

    /**
     * @return mixed
     */
    public function getUpdatedDate()
    {
        return $this->updated_date;
    }

    /**
     * @param mixed $updated_date
     */
    public function setUpdatedDate($updated_date)
    {
        $this->updated_date = $updated_date;
    }

    /**
     * @return mixed
     */
    public function getUpdatedBy()
    {
        return $this->updated_by;
    }

    /**
     * @param mixed $updated_by
     */
    public function setUpdatedBy($updated_by)
    {
        $this->updated_by = $updated_by;
    }

    /**
     * @return mixed
     */
    public function getUpdatedByIp()
    {
        return $this->updated_by_ip;
    }

    /**
     * @param mixed $updated_by_ip
     */
    public function setUpdatedByIp($updated_by_ip)
    {
        $this->updated_by_ip = $updated_by_ip;
    }

    /**
     * @return mixed
     */
    public function getDeletedDate()
    {
        return $this->deleted_date;
    }

    /**
     * @param mixed $deleted_date
     */
    public function setDeletedDate($deleted_date)
    {
        $this->deleted_date = $deleted_date;
    }

    /**
     * @return mixed
     */
    public function getDeletedBy()
    {
        return $this->deleted_by;
    }

    /**
     * @param mixed $deleted_by
     */
    public function setDeletedBy($deleted_by)
    {
        $this->deleted_by = $deleted_by;
    }

    /**
     * @return mixed
     */
    public function getDeletedByIp()
    {
        return $this->deleted_by_ip;
    }

    /**
     * @param mixed $deleted_by_ip
     */
    public function setDeletedByIp($deleted_by_ip)
    {
        $this->deleted_by_ip = $deleted_by_ip;
    }

}