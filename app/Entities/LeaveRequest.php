<?php
namespace App\Entities;
use Doctrine\ORM\Mapping AS ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="TB_LEAVE_REQUEST")
 */

class LeaveRequest
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer" , length=11)
     */
    protected $company_id;
    /**
     * @ORM\Id
     * @ORM\Column(type="integer" , length=11)
     */
    protected $leave_request_id;
    /**
     * @ORM\Column(type="integer" , length=11, nullable = true)
     */
    protected $profile_id;
    /**
     * @ORM\Column(type="string" , length=30, nullable = true)
     */
    protected $leave_request_no;
    /**
     * @ORM\Column(type="integer" , length=11, nullable = true)
     */
    protected $leave_detail_id;
    /**
     * @ORM\Column(type="string" , length=1, nullable = true)
     */
    protected $leave_detail_type;
    /**
     * @ORM\Column(type="string" , length=1, nullable = true)
     */
    protected $leave_request_type;
    /**
     * @ORM\Column(type="date" )
     */
    protected $leave_start_date;
    /**
     * @ORM\Column(type="date" )
     */
    protected $leave_end_date;
    /**
     * @ORM\Column(type="string" , length=4, nullable = true)
     */
    protected $leave_start_time;
    /**
     * @ORM\Column(type="string" , length=4, nullable = true)
     */
    protected $leave_end_time;
    /**
     * @ORM\Column(type="float" , nullable = true)
     */
    protected $leave_total_day;
    /**
     * @ORM\Column(type="string" , length=1, nullable = true)
     */
    protected $leave_approve_type;
    /**
     * @ORM\Column(type="integer" , length=11, nullable = true)
     */
    protected $leave_approve_count;
    /**
     * @ORM\Column(type="date" )
     */
    protected $requested_date;
    /**
     * @ORM\Column(type="integer" , length=11, nullable = true)
     */
    protected $requested_by;
    /**
     * @ORM\Column(type="string" , length=30, nullable = true)
     */
    protected $requested_by_ip;
    /**
     * @ORM\Column(type="date" )
     */
    protected $rejected_date;
    /**
     * @ORM\Column(type="integer" , length=11, nullable = true)
     */
    protected $rejected_by;
    /**
     * @ORM\Column(type="string" , length=30, nullable = true)
     */
    protected $rejected_by_ip;

    /**
     * @return mixed
     */
    public function getCompanyId()
    {
        return $this->company_id;
    }

    /**
     * @param mixed $company_id
     */
    public function setCompanyId($company_id)
    {
        $this->company_id = $company_id;
    }

    /**
     * @return mixed
     */
    public function getLeaveRequestId()
    {
        return $this->leave_request_id;
    }

    /**
     * @param mixed $leave_request_id
     */
    public function setLeaveRequestId($leave_request_id)
    {
        $this->leave_request_id = $leave_request_id;
    }

    /**
     * @return mixed
     */
    public function getProfileId()
    {
        return $this->profile_id;
    }

    /**
     * @param mixed $profile_id
     */
    public function setProfileId($profile_id)
    {
        $this->profile_id = $profile_id;
    }

    /**
     * @return mixed
     */
    public function getLeaveRequestNo()
    {
        return $this->leave_request_no;
    }

    /**
     * @param mixed $leave_request_no
     */
    public function setLeaveRequestNo($leave_request_no)
    {
        $this->leave_request_no = $leave_request_no;
    }

    /**
     * @return mixed
     */
    public function getLeaveDetailId()
    {
        return $this->leave_detail_id;
    }

    /**
     * @param mixed $leave_detail_id
     */
    public function setLeaveDetailId($leave_detail_id)
    {
        $this->leave_detail_id = $leave_detail_id;
    }

    /**
     * @return mixed
     */
    public function getLeaveDetailType()
    {
        return $this->leave_detail_type;
    }

    /**
     * @param mixed $leave_detail_type
     */
    public function setLeaveDetailType($leave_detail_type)
    {
        $this->leave_detail_type = $leave_detail_type;
    }

    /**
     * @return mixed
     */
    public function getLeaveRequestType()
    {
        return $this->leave_request_type;
    }

    /**
     * @param mixed $leave_request_type
     */
    public function setLeaveRequestType($leave_request_type)
    {
        $this->leave_request_type = $leave_request_type;
    }

    /**
     * @return mixed
     */
    public function getLeaveStartDate()
    {
        return $this->leave_start_date;
    }

    /**
     * @param mixed $leave_start_date
     */
    public function setLeaveStartDate($leave_start_date)
    {
        $this->leave_start_date = $leave_start_date;
    }

    /**
     * @return mixed
     */
    public function getLeaveEndDate()
    {
        return $this->leave_end_date;
    }

    /**
     * @param mixed $leave_end_date
     */
    public function setLeaveEndDate($leave_end_date)
    {
        $this->leave_end_date = $leave_end_date;
    }

    /**
     * @return mixed
     */
    public function getLeaveStartTime()
    {
        return $this->leave_start_time;
    }

    /**
     * @param mixed $leave_start_time
     */
    public function setLeaveStartTime($leave_start_time)
    {
        $this->leave_start_time = $leave_start_time;
    }

    /**
     * @return mixed
     */
    public function getLeaveEndTime()
    {
        return $this->leave_end_time;
    }

    /**
     * @param mixed $leave_end_time
     */
    public function setLeaveEndTime($leave_end_time)
    {
        $this->leave_end_time = $leave_end_time;
    }

    /**
     * @return mixed
     */
    public function getLeaveTotalDay()
    {
        return $this->leave_total_day;
    }

    /**
     * @param mixed $leave_total_day
     */
    public function setLeaveTotalDay($leave_total_day)
    {
        $this->leave_total_day = $leave_total_day;
    }

    /**
     * @return mixed
     */
    public function getLeaveApproveType()
    {
        return $this->leave_approve_type;
    }

    /**
     * @param mixed $leave_approve_type
     */
    public function setLeaveApproveType($leave_approve_type)
    {
        $this->leave_approve_type = $leave_approve_type;
    }

    /**
     * @return mixed
     */
    public function getLeaveApproveCount()
    {
        return $this->leave_approve_count;
    }

    /**
     * @param mixed $leave_approve_count
     */
    public function setLeaveApproveCount($leave_approve_count)
    {
        $this->leave_approve_count = $leave_approve_count;
    }

    /**
     * @return mixed
     */
    public function getRequestedDate()
    {
        return $this->requested_date;
    }

    /**
     * @param mixed $requested_date
     */
    public function setRequestedDate($requested_date)
    {
        $this->requested_date = $requested_date;
    }

    /**
     * @return mixed
     */
    public function getRequestedBy()
    {
        return $this->requested_by;
    }

    /**
     * @param mixed $requested_by
     */
    public function setRequestedBy($requested_by)
    {
        $this->requested_by = $requested_by;
    }

    /**
     * @return mixed
     */
    public function getRequestedByIp()
    {
        return $this->requested_by_ip;
    }

    /**
     * @param mixed $requested_by_ip
     */
    public function setRequestedByIp($requested_by_ip)
    {
        $this->requested_by_ip = $requested_by_ip;
    }

    /**
     * @return mixed
     */
    public function getRejectedDate()
    {
        return $this->rejected_date;
    }

    /**
     * @param mixed $rejected_date
     */
    public function setRejectedDate($rejected_date)
    {
        $this->rejected_date = $rejected_date;
    }

    /**
     * @return mixed
     */
    public function getRejectedBy()
    {
        return $this->rejected_by;
    }

    /**
     * @param mixed $rejected_by
     */
    public function setRejectedBy($rejected_by)
    {
        $this->rejected_by = $rejected_by;
    }

    /**
     * @return mixed
     */
    public function getRejectedByIp()
    {
        return $this->rejected_by_ip;
    }

    /**
     * @param mixed $rejected_by_ip
     */
    public function setRejectedByIp($rejected_by_ip)
    {
        $this->rejected_by_ip = $rejected_by_ip;
    }

}