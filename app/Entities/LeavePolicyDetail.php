<?php
namespace App\Entities;
use Doctrine\ORM\Mapping AS ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="TB_LEAVE_POLICY_DETAIL")
 */

class LeavePolicyDetail
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer" , length=11)
     */
    protected $company_id;
    /**
     * @ORM\Id
     * @ORM\Column(type="integer" , length=11)
     */
    protected $leave_policy_id;
    /**
     * @ORM\Column(type="string" , length=1, nullable = true)
     */
    protected $leave_detail_type;
    /**
     * @ORM\Column(type="string" , length=1, nullable = true)
     */
    protected $leave_detail_compensate;
   /**
    * @ORM\Column(type="integer" , length=11, nullable = true)
    */
    protected $leave_detail_limit_compensate;
    /**
     * @ORM\Column(type="integer" , length=11, nullable = true)
     */
    protected $leave_detail_count;
    /**
     * @ORM\Column(type="string" , length=1, nullable = true)
     */
    protected $leave_detail_prorate;
    /**
     * @ORM\Column(type="string" , length=1, nullable = true)
     */
    protected $leave_detail_ignore_probation;
    /**
     * @ORM\Column(type="string" , length=1, nullable = true)
     */
    protected $leave_carry_over;
    /**
     * @ORM\Column(type="string" , length=1, nullable = true)
     */
    protected $leave_carry_over_type;
    /**
     * @ORM\Column(type="integer" , length=11, nullable = true)
     */
    protected $leave_carry_over_max;
    /**
     * @ORM\Column(type="integer" , length=11, nullable = true)
     */
    protected $leave_carry_over_count;
    /**
     * @ORM\Column(type="datetime" )
     */
    protected $mapped_date;
    /**
     * @ORM\Column(type="integer" , length=11, nullable = true)
     */
    protected $mapped_by;
    /**
     * @ORM\Column(type="string" , length=30, nullable = true)
     */
    protected $mapped_by_ip;

    /**
     * @return mixed
     */
    public function getCompanyId()
    {
        return $this->company_id;
    }

    /**
     * @param mixed $company_id
     */
    public function setCompanyId($company_id)
    {
        $this->company_id = $company_id;
    }

    /**
     * @return mixed
     */
    public function getLeavePolicyId()
    {
        return $this->leave_policy_id;
    }

    /**
     * @param mixed $leave_policy_id
     */
    public function setLeavePolicyId($leave_policy_id)
    {
        $this->leave_policy_id = $leave_policy_id;
    }

    /**
     * @return mixed
     */
    public function getLeaveDetailType()
    {
        return $this->leave_detail_type;
    }

    /**
     * @param mixed $leave_detail_type
     */
    public function setLeaveDetailType($leave_detail_type)
    {
        $this->leave_detail_type = $leave_detail_type;
    }

    /**
     * @return mixed
     */
    public function getLeaveDetailCompensate()
    {
        return $this->leave_detail_compensate;
    }

    /**
     * @param mixed $leave_detail_compensate
     */
    public function setLeaveDetailCompensate($leave_detail_compensate)
    {
        $this->leave_detail_compensate = $leave_detail_compensate;
    }

    /**
     * @return mixed
     */
    public function getLeaveDetailLimitCompensate()
    {
        return $this->leave_detail_limit_compensate;
    }

    /**
     * @param mixed $leave_detail_limit_compensate
     */
    public function setLeaveDetailLimitCompensate($leave_detail_limit_compensate)
    {
        $this->leave_detail_limit_compensate = $leave_detail_limit_compensate;
    }

    /**
     * @return mixed
     */
    public function getLeaveDetailCount()
    {
        return $this->leave_detail_count;
    }

    /**
     * @param mixed $leave_detail_count
     */
    public function setLeaveDetailCount($leave_detail_count)
    {
        $this->leave_detail_count = $leave_detail_count;
    }

    /**
     * @return mixed
     */
    public function getLeaveDetailProrate()
    {
        return $this->leave_detail_prorate;
    }

    /**
     * @param mixed $leave_detail_prorate
     */
    public function setLeaveDetailProrate($leave_detail_prorate)
    {
        $this->leave_detail_prorate = $leave_detail_prorate;
    }

    /**
     * @return mixed
     */
    public function getLeaveDetailIgnoreProbation()
    {
        return $this->leave_detail_ignore_probation;
    }

    /**
     * @param mixed $leave_detail_ignore_probation
     */
    public function setLeaveDetailIgnoreProbation($leave_detail_ignore_probation)
    {
        $this->leave_detail_ignore_probation = $leave_detail_ignore_probation;
    }

    /**
     * @return mixed
     */
    public function getLeaveCarryOver()
    {
        return $this->leave_carry_over;
    }

    /**
     * @param mixed $leave_carry_over
     */
    public function setLeaveCarryOver($leave_carry_over)
    {
        $this->leave_carry_over = $leave_carry_over;
    }

    /**
     * @return mixed
     */
    public function getLeaveCarryOverType()
    {
        return $this->leave_carry_over_type;
    }

    /**
     * @param mixed $leave_carry_over_type
     */
    public function setLeaveCarryOverType($leave_carry_over_type)
    {
        $this->leave_carry_over_type = $leave_carry_over_type;
    }

    /**
     * @return mixed
     */
    public function getLeaveCarryOverMax()
    {
        return $this->leave_carry_over_max;
    }

    /**
     * @param mixed $leave_carry_over_max
     */
    public function setLeaveCarryOverMax($leave_carry_over_max)
    {
        $this->leave_carry_over_max = $leave_carry_over_max;
    }

    /**
     * @return mixed
     */
    public function getLeaveCarryOverCount()
    {
        return $this->leave_carry_over_count;
    }

    /**
     * @param mixed $leave_carry_over_count
     */
    public function setLeaveCarryOverCount($leave_carry_over_count)
    {
        $this->leave_carry_over_count = $leave_carry_over_count;
    }

    /**
     * @return mixed
     */
    public function getMappedDate()
    {
        return $this->mapped_date;
    }

    /**
     * @param mixed $mapped_date
     */
    public function setMappedDate($mapped_date)
    {
        $this->mapped_date = $mapped_date;
    }

    /**
     * @return mixed
     */
    public function getMappedBy()
    {
        return $this->mapped_by;
    }

    /**
     * @param mixed $mapped_by
     */
    public function setMappedBy($mapped_by)
    {
        $this->mapped_by = $mapped_by;
    }

    /**
     * @return mixed
     */
    public function getMappedByIp()
    {
        return $this->mapped_by_ip;
    }

    /**
     * @param mixed $mapped_by_ip
     */
    public function setMappedByIp($mapped_by_ip)
    {
        $this->mapped_by_ip = $mapped_by_ip;
    }

}